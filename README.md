# editorAnnotate

#### 介绍
skyeyeAnnotate是一款基于jQuery封装的HTML文章批注插件，帮助开发者轻松实现类似word的文本批注。我们需要在一个或多个网页中添加批注信息的时候这个插件可以简化我们许多工作。使用者可以随意选择文本进行批注以及批注回复，支持批注历史查看等功能。支持客户合同批注、试卷批阅等多种场景。

#### 软件架构
基于jQuery封装的HTML批注插件。

#### 演示地址
http://www.jq22.com/demo/editorAnnotate202008032310/

#### 安装教程
导入前端开发工具，运行index.html即可。

#### 交流

- 作者本人承诺，知识星球人数达到1000人，即开放所有功能模块源代码(仅供星球内部成员使用)
- [Skyeye云系列资料](https://docs.qq.com/doc/DYUxuT3pSdGhVVXFC)
- **接前端 / Java后端等课程设计以及毕设制作，有需要的可加下方微信**

### 沟通交流

| 企业部署版 | 作者微信 |  QQ群    | Java学习 知识星球 |
| ------------ | ------------ | ------------ | ------------ |
| ![](https://gitee.com/doc_wei01/skyeye/raw/company_server/images/mindMap/知识星球.png)  | ![](https://gitee.com/doc_wei01/skyeye/raw/company_server/images/mindMap/chatgpt的微信.jpg)  | ![](https://gitee.com/doc_wei01/skyeye/raw/company_server/images/mindMap/Skyeye智能制造云办公官方①群群二维码.png)  | ![输入图片说明](images/Java%E5%AD%A6%E4%B9%A0%E7%9F%A5%E8%AF%86%E6%98%9F%E7%90%83.png) |

#### 文档参数
|key|参数|含义|
| ------------- | ------------- | ------------- |
|id|默认：null|初始化对象|
|width|默认：240|批注盒子默认宽度|
|userName|默认：skyeye|添加批注的用户的名称|
|userImage|默认：skyeye-editor/images/no-userphoto.png|添加批注的用户的logo|
|data|默认：[]|默认回显的数据|
|selectedElem|默认：null|当前选中的文本对象，`只供插件运行时使用`|
|addIconSrc|默认：skyeye-editor/images/add-icon.png|新增批注时展示的图标|
|whetherLoadData|默认：false|是否已经加载数据，根据数据的数量进行判断，`只供插件运行时使用`|
|resetContentBox|默认：true|是否重置内容对象宽度|
|annotateId|默认：null|批注盒子的id|
|addAnnotate|默认：true|是否可以添加新的批注
|edit|默认：true|是否可以编辑

#### 插件方法
|方法|入参介绍|含义|
| ------------- | ------------- | ------------- |
|$(dom).skyeyeAnnotate()|入参见上面文档参数|插件初始化方法|
|$(dom).getAnnotateData()|-|获取当前内容对象所有的批注数据|

#### 使用方法

```
// 初始化
var annotate = $("#container").skyeyeAnnotate({});
// 获取数据
var data = annotate.getAnnotateData();
```

#### 效果图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/223527_99e760f7_1541735.png "屏幕截图.png")
